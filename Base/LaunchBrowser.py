from selenium import webdriver
import Library
import os

class LaunchBrowser:
    def startBrowser(self):
        driver=webdriver.Chrome(os.getcwd()+"\\Driver\\chromedriver.exe")
        readobj = Library.readFile.readFileData(driver)
        driver.get(readobj.readConfigData('QA2Urls','RealSimple'))
        driver.set_page_load_timeout(20)
        driver.implicitly_wait(20)
        driver.maximize_window()
        return driver

    def closeBrowser(self,driver):
        driver.close()