from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

class checkVisibility:
    def __init__(self,driver):
        print("This is constructor")
        self.driver = driver

    def checkForElement(self,xPath,elementName):
        print("Checking the visibility of "+elementName)
        if(self.driver.find_element_by_xpath(xPath).is_displayed()):
            print(elementName+" is displayed")
        else:
            print(elementName+" is not displayed")

    def waitForElement(self,xPath,elementName):
        print("Waiting for "+elementName+" to be visible")
        wait = WebDriverWait(self.driver, 10)
        men_menu = wait.until(ec.visibility_of_element_located((By.XPATH,xPath)))