import json
import configparser
import os

class readFileData:
    def __init__(self,driver):
        self.driver = driver

    def readObjRepo(self,deviceType,appName,FilePath):
        object_Repo_filePath = FilePath + appName + "\\" + deviceType + ".json";
        with open(object_Repo_filePath) as json_file:
            data = json.load(json_file)
        dict_ObjectRepo = (data[appName.lower()])
        return dict_ObjectRepo
#readObjRepo("Desktop","RealSimple","C:\\Users\\pandas\\ShopNationAutomation\\Resources\\ObjectRepository\\")

    def readConfigData(self,Section, Key):
        config = configparser.ConfigParser()
        config.read(os.getcwd()+"\\ConfigFiles\\ApplicationURLs.cfg")
        return config.get(Section, Key)