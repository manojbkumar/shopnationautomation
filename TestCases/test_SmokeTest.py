from Base import LaunchBrowser
import Library
import os

def test_ValidateHomePageSmoke():
    print("The Driver is launching")
    launchobj = LaunchBrowser.LaunchBrowser()
    driver = launchobj.startBrowser()
    print("Driver launched")
    readobj = Library.readFile.readFileData(driver)
    dict_repo = readobj.readObjRepo("Desktop", "RealSimple", os.getcwd()+"\\Resources\\ObjectRepository\\")
    print(dict_repo['HomePage.Logo'])
    checkobj = Library.checkVisibility.checkVisibility(driver)
    checkobj.waitForElement(dict_repo['HomePage.Logo'],"HomePage Logo")
    checkobj.checkForElement(dict_repo['HomePage.Logo'],"HomePage Logo")
    checkobj.checkForElement(dict_repo['HomePage.categoryhomepagae'], "Category Home Link")
    clickobj = Library.clickModule.clickModule(driver)
    clickobj.clickOnElement(dict_repo['HomePage.categoryhomepagae'], "Category Home Link")
    print("Driver is closing")
    launchobj.closeBrowser(driver)